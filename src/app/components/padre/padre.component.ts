import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {
mensajePadre = 'El punto de salida hacia todo logro es el deseo'
mensaje2 = 'Ama la vida que tienes para poder vivir la vida que amas'
numeroPadre= 3.1416892038348;
mensaje3= 'Hay que endurecerse sin perder jamás la ternura'
mensaje4= 'Aprendí que no se puede dar marcha atrás, que la esencia de la vida es ir hacia adelante. La vida, en realidad, es una calle de sentido único'
  constructor() { }

  ngOnInit(): void {
  }

}
